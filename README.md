# sugardump

A simple example of using the Sugar REST API to export a subset of fields to a json file.

```
Usage: sugar_dump.py [OPTIONS]

Options:
  --username TEXT                 [env var: SUGAR_USER]
  --password TEXT                 [env var: SUGAR_PASSWORD]
```