import json
import logging
from typing import Union

import requests
import typer

logging.basicConfig(level="INFO", format="%(asctime)s %(message)s")
logger = logging.getLogger(__name__)

auth_url = "https://crossref.sugarondemand.com/rest/v11/oauth2/token"
accounts_url = "https://crossref.sugarondemand.com/rest/v11/Accounts/filter"

max_num = 500

sugar_fields = [
    "account_type",
    "agent_name",
    "alt_name_c",
    "annual_fee_c",
    "annual_revenue",
    "annualfee_c",
    "annual_fee_c",
    "accumulated_waiver_dollars_c",
    "accumulatedwaiver_c",
    "active_c",
    "archiving_provider_c",
    "billing_address_country",
    "billing_address_country_text_c",
    "billing_address_city",
    "cross_check",
    "cross_mark",
    "crossref_member_id_c",
    "currentfee_c",
    "date_joined",
    "date_joined_for_workflows_c",
    "date_modified",
    "date_mod_c",
    "disabled_c",
    "doi_prefixes_c",
    "id",
    "inactive_reason_c",
    "inactive_yes_no_c",
    "intacct_customer_id",
    "intacct_totaldue",
    "last_deposit_c",
    "lead_organisation_url_c",
    "manuscript_submission_system_c",
    "member_id_c",
    "name",
    "next_renewal_date",
    "non_profit",
    "ojapkp_c",
    "org_type_c",
    "organisation_name_c",
    "organization_type_c",
    "orgwebsite_c",
    "other_archiving_provider",
    "ownership",
    "parent_name",
    "payment_portal_c",
    "platform_provider_c",
    "publishown_c",
    "publishrep_c",
    "sponsor_content_registration_c",
    "sponsor_name",
    "sponsoring_relationship_type",
    "suspended_yes_no_c",
    "systemusername_c",
    "termination_date_c",
    "total_dois_account_c",
    "website",
]

payload_template = {
    "max_num": 10,
    "offset": 0,
    "fields": ",".join(sugar_fields),
    "order_by": "date_entered",
    "favorites": False,
    "my_items": False,
}

common_headers = {
    "Cache-Control": "no-cache",
    "Content-Type": "application/json",
}


def get_sugar_records(
    max_num: int = 10, offset: int = 0, oauth_headers: dict = None
) -> Union[dict, bool]:
    if offset >= 27000:
        logger.warning("stopping at debug level")
        return False
    logger.info(f"getting records {offset}-{offset + max_num}")
    payload = json.dumps(payload_template | {"max_num": max_num, "offset": offset})
    records = requests.request(
        "POST", accounts_url, headers=oauth_headers, data=payload
    ).json()
    return False if records["next_offset"] == -1 else records


def get_oauth_headers(username: str = None, password: str = None) -> dict:

    payload = json.dumps(
        {
            "grant_type": "password",
            "client_id": "sugar",
            "client_secret": "",
            "username": username,
            "password": password,
            "platform": "custom_api",
        }
    )

    response = requests.request(
        "POST", auth_url, headers=common_headers, data=payload
    ).json()
    return {
        "Authorization": f"Bearer {response['access_token']}",
    }


def main(
    username: str = typer.Option(None, envvar="SUGAR_USER"),
    password: str = typer.Option(
        None,
        envvar="SUGAR_PASSWORD",
        prompt=True,
        hide_input=True,
        confirmation_prompt=True,
    ),
):

    items = []
    offset = 0

    oauth_headers = common_headers | get_oauth_headers(
        username=username, password=password
    )

    while batch := get_sugar_records(
        max_num=max_num, offset=offset, oauth_headers=oauth_headers
    ):
        items += batch["records"]
        offset = batch["next_offset"]

    logger.info(len(items))
    print(json.dumps(items, indent=4))


if __name__ == "__main__":
    typer.run(main)
